/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arkessa.avnetprovisioningmvn;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 *
 * @author barry
 */
public class MonikersJSON {
    
    @JsonProperty("IsSuccess")
    private boolean success;
    
    @JsonProperty("Message")
    private String message;

    @JsonProperty("SIMVersions")
    private List<String> SIMVersions;
    
    @JsonProperty("NTMIProfiles")
    private List<String> NTMIProfiles;
    
    @JsonProperty("HLRProfiles")
    private List<String> HLRProfiles;
    
    @JsonCreator
    
//    public AboutJson(@JsonProperty("userMgmtVersion") String userMgmtVersion,
//                    @JsonProperty("webVersion") String webVersion, 
//                    @JsonProperty("javaVersion") String javaVersion, 
//                    @JsonProperty("osVersion") String osVersion, 
//                    @JsonProperty("serverVersion") String serverVersion) {
//        this.userMgmtVersion = userMgmtVersion;
//        this.webVersion = webVersion;
//        this.javaVersion = javaVersion;
//        this.osVersion = osVersion;
//        this.serverVersion = serverVersion;
//    }


}
