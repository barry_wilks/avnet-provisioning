/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arkessa.avnetprovisioningmvn;

import com.google.common.collect.ImmutableMap;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author barry
 */
public class CsvProcessor {

    List<SimIdentification> processFile(File uploadFile) {
        Map<String, Integer> headerMap = null;
        List<SimIdentification> result = new ArrayList<>();
        try {
            List<String> readAllLines = Files.readAllLines(uploadFile.toPath());
           
            boolean firstLine = true;
            for (String line : readAllLines)
            {
                if (firstLine)
                {
                    headerMap = checkHeaders(line.split(","));
                    firstLine = false;
                    continue;
                }
                
                result.add(splitAndTrim(headerMap, line));
                
            }

        } catch (IOException ex) {
            throw new RuntimeException("IO Error while processing file");
        } catch (RuntimeException rex) {
            throw new RuntimeException("Error while processing file: " + rex.getMessage());
        }

        return result;
    }

    private SimIdentification splitAndTrim(Map<String, Integer> headerMap, String line) {
        String[] split = line.split(",");
        return new SimIdentification(split[headerMap.get("MSISDN")], split[headerMap.get("IMSI")]);
    }

    private Map<String, Integer> checkHeaders(String[] headers) {
        if (!headers[0].trim().toUpperCase().equals("MSISDN") && !headers[0].trim().toUpperCase().equals("IMSI") ||
                !headers[1].trim().toUpperCase().equals("MSISDN") && !headers[1].trim().toUpperCase().equals("IMSI"))
            throw new RuntimeException("Must supply MSISDN and IMSI in upload CSV");
        
        if (headers[0].trim().toUpperCase().equals("IMSI"))
            return ImmutableMap.of("IMSI", 0, "MSISDN", 1);
        else
            return ImmutableMap.of("IMSI", 1, "MSISDN", 0);
    }    
}
